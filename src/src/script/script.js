window.onload = function() {

    document.getElementById("save_customer").onmousedown = function() {

        var first_name = document.getElementById("first_name");
        var last_name = document.getElementById("last_name");
        var birthdate = document.getElementById("birthdate");
        var address = document.getElementById("address");
        var city = document.getElementById("city");
        var passport = document.getElementById("passport");
        var phone = document.getElementById("phone");

        var newRow = document.getElementById("customer_list").insertRow();

        newRow.insertCell().appendChild(document.createTextNode(first_name.value));
        newRow.insertCell().appendChild(document.createTextNode(last_name.value));
        newRow.insertCell().appendChild(document.createTextNode(birthdate.value));
        newRow.insertCell().appendChild(document.createTextNode(address.value));
        newRow.insertCell().appendChild(document.createTextNode(city.value));
        newRow.insertCell().appendChild(document.createTextNode(passport.value));
        newRow.insertCell().appendChild(document.createTextNode(phone.value));

        first_name.value = "";
        last_name.value = "";
        birthdate.value = "";
        address.value = "";
        city.value = "";
        passport.value = "";
        phone.value = "";

    }

}